unit uCliente;

interface

type TCliente = class
  private
      TCLI_CLIENTE_PK: Integer;
      TCLI_RAZAO_SOCIAL: String;
      TCLI_VENDENDOR_FK: Integer;
  public
      property FTCLI_CLIENTE_PK    :Integer    read TCLI_CLIENTE_PK    write TCLI_CLIENTE_PK;
      property FTCLI_RAZAO_SOCIAL  :String     read TCLI_RAZAO_SOCIAL  write TCLI_RAZAO_SOCIAL;
      property FTCLI_VENDENDOR_FK  :Integer    read TCLI_VENDENDOR_FK  write TCLI_VENDENDOR_FK;
end;

implementation
 { TCliente }
end.
 